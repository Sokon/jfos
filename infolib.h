#ifndef __INFO_H__
#define __INFO_H__

extern const char *info_cpu_features[];

char *info_get_cpu_model(void);
unsigned int info_get_cpu_features(void);

#endif
