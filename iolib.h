#ifndef __IOLIB_H__
#define __IOLIB_H__

#include <stdint.h>
#include <stdarg.h>

enum VGA_COLORS {
	BLACK,
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,
	BROWN,
	LGRAY,
	DGRAY,
	LBLUE,
	LGREEN,
	LCYAN,
	LRED,
	LMAGENTA,
	LBROWN,
	WHITE
};
typedef enum VGA_COLORS io_color_t;

enum CUR_TYPE {
	BLOCK,
	UNDERLINE,
};
typedef enum CUR_TYPE io_curtype_t;

/*
 * gen - generate
 * mov - move
 * clr - clear
 *
 * set - set property
 * get - get property
 * set <=> get
 *
 * put - write value 
 * rcv - read value
 * put <=> rcv
 */

uint16_t io_gen_char_vga(unsigned char c, io_color_t fg, io_color_t bg);
void io_put_char_at(uint16_t vga_character, uint16_t x, uint16_t y);
void io_set_buffer_dimensions(uint16_t x, uint16_t y);
void io_set_buffer_colors(io_color_t fg, io_color_t bg);
void io_put_byte_port(uint16_t port, uint8_t value);
uint8_t io_rcv_byte_port(uint16_t port);
void io_dis_cursor(void);
void io_ena_cursor(io_curtype_t type);
void io_mov_cursor(uint16_t x, uint16_t y);
uint16_t io_get_cursor_position(void);
void io_set_cursor_shape(io_curtype_t type);
void io_put_char(unsigned char c);
void io_put_str(const char *s);
void io_clr_screen(void);
void io_put_point(uint16_t x, uint16_t y, io_color_t color);
uint32_t io_put_formatted(const char *fmt, ...);

/* convenience macros */
#define putc(x) io_put_char(x)
#define puts(x) io_put_str(x)
#define cls() io_clr_screen()
#define print(x, ...) io_put_formatted(x, __VA_ARGS__)

#endif
