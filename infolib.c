#include <stdint.h>
#include <cpuid.h>

#include "infolib.h"

const char *info_cpu_features[] = {
	"FPU", "VME", "DE", "PSE",
	"TSC", "MSR", "PAE", "MCE",
	"CX8", "APIC", "-", "SEP",
	"MTRR", "PGE", "MCA", "CMOV",
	"PAT", "PSE36", "PSN", "CLFLUSH",
	"-", "DS", "ACPI", "MMX",
	"FXSR", "SSE", "SSE2", "SS",
	"HTT", "TM", "IA64", "PBE"

};

char *info_get_cpu_model(void) {
	static char model[13];
	uint32_t reg[3];
	void *none;
	__cpuid(0, none, reg[0], reg[2], reg[1]);
	for(int i=0; i<=11; i++){
		model[i] = *((char*)reg+i);
	}
	model[12]='\0';
	return model;
}

unsigned int info_get_cpu_features(void) {
	unsigned int edx = 0;
	void *none;
	__get_cpuid(1, none, none, none, &edx);
	return edx;
}
