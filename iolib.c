#include "iolib.h"
#include "stringlib.h"
#include <stddef.h>
#include <stdarg.h>


uint16_t *io_vga_buffer 	= (uint16_t*)0xB8000;
uint16_t io_vga_buffer_width 	= 80;
uint16_t io_vga_buffer_height 	= 25;
uint16_t io_vga_buffer_fg 	= MAGENTA;
uint16_t io_vga_buffer_bg 	= BLACK;
io_curtype_t io_cursor_shape 	= BLOCK;
uint16_t io_cursor_x 		= 0,
	 io_cursor_y 		= 0;

#define WHOLE_BUFFER io_vga_buffer_height*io_vga_buffer_width

uint16_t io_gen_char_vga(unsigned char c, io_color_t fg, io_color_t bg){
	return (uint16_t)c | (uint16_t)(fg | bg << 4) << 8;
}

void io_put_char_at(uint16_t vga_character, uint16_t x, uint16_t y){
	io_vga_buffer[y*io_vga_buffer_width+x] = vga_character;
}

void io_set_buffer_dimensions(uint16_t width, uint16_t height){
	io_vga_buffer_width = width;
	io_vga_buffer_height = height;
}

void io_set_buffer_colors(io_color_t fg, io_color_t bg){
	io_vga_buffer_fg = fg;
	io_vga_buffer_bg = bg;
}

void io_put_byte_port(uint16_t port, uint8_t value){
	asm volatile ("outb %0, %1"
			:
			: "a"(value), "Nd"(port));
}

uint8_t io_rcv_byte_port(uint16_t port){
	uint8_t ret = 255;
	asm volatile ("inb %1, %0"
			: "=a"(ret)
			: "Nd"(port));
	return ret;
}

void io_dis_cursor(void){
	io_put_byte_port(0x3D4, 0x0A);
	io_put_byte_port(0x3D5, 0x20);
}

void io_ena_cursor(io_curtype_t type){
	uint8_t s = 0, e = 0;
	switch(type) {
		case BLOCK:
			s = 0x00;
			e = 0x0F;
			break;
		case UNDERLINE:
			s = 0x0E;
			e = 0x0F;
			break;
	}
	io_put_byte_port(0x3D4, 0x0A);
	io_put_byte_port(0x3D5, (io_rcv_byte_port(0x3D5) & 0xC0) | s);

	io_put_byte_port(0x3D4, 0x0B);
	io_put_byte_port(0x3D5, (io_rcv_byte_port(0x3D5) & 0xE0) | e);
}

void io_mov_cursor(uint16_t x, uint16_t y){
	uint16_t new_position = y * io_vga_buffer_width + x;

	io_put_byte_port(0x3D4, 0x0F);
	io_put_byte_port(0x3D5, (uint8_t)(new_position & 0xFF));
	io_put_byte_port(0x3D4, 0x0E);
	io_put_byte_port(0x3D5, (uint8_t)((new_position >> 8) & 0xFF));
}

void io_set_cursor_shape(io_curtype_t type){
	io_cursor_shape = type;
}

uint16_t io_get_cursor_position(void){
	uint16_t curr_position = 0;
	io_put_byte_port(0x3D4, 0x0F);
	curr_position |= io_rcv_byte_port(0x3D5);
	io_put_byte_port(0x3D4, 0x0E);
	curr_position |= ((uint16_t)io_rcv_byte_port(0x3D5)) << 8;
	return curr_position;
}

void io_put_char(unsigned char c){
	uint16_t curr_position = io_get_cursor_position();
	switch(c){
		case '\x11':
			--curr_position;
			break;
		case '\x12':
			curr_position -= io_vga_buffer_width;
			break;
		case '\x13':
			++curr_position;
			break;
		case '\x14':
			curr_position += io_vga_buffer_width;
			break;
		case '\n':
			io_vga_buffer[curr_position] = io_gen_char_vga(
					'\0',
					io_vga_buffer_fg,
					io_vga_buffer_bg);
			curr_position += io_vga_buffer_width
				- (curr_position % io_vga_buffer_width);
			break;
		default:
			io_vga_buffer[curr_position++] = io_gen_char_vga(
					c,
					io_vga_buffer_fg,
					io_vga_buffer_bg);
	}

	io_put_byte_port(0x3D4, 0x0F);
	io_put_byte_port(0x3D5, (uint8_t)(curr_position & 0xFF));
	io_put_byte_port(0x3D4, 0x0E);
	io_put_byte_port(0x3D5, (uint8_t)((curr_position >> 8) & 0xFF));
}

void io_put_str(const char *s){
	io_dis_cursor();
	for(uint32_t i=0; s[i] != '\0'; i++){
		io_put_char(s[i]);
	}
	io_ena_cursor(io_cursor_shape);
}


void io_clr_screen(void){
	for(uint16_t i=0; i <= WHOLE_BUFFER; i++){
		io_vga_buffer[i] = io_gen_char_vga('\0', io_vga_buffer_fg, io_vga_buffer_bg);
	}
	io_mov_cursor(0,0);
}

void io_put_point(uint16_t x, uint16_t y, io_color_t color){
	io_vga_buffer[y*io_vga_buffer_width+x] = color << 12;
}

uint32_t io_put_formatted(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);

	uint32_t i = 0;
	for(i; fmt[i]!='\0'; i++){
		if(fmt[i] == '~'){
			switch(fmt[i+1]){
				case 'd':
					uint32_t d = va_arg(args, uint32_t);
					puts(str_itoa_dec(d));
					break;
				case 'b':
					uint32_t b = va_arg(args, uint32_t);
					puts(str_itoa_bin(b));
					break;
				case 'x':
					uint32_t x = va_arg(args, uint32_t);
					puts(str_itoa_hex(x));
					break;
				case 'o':
					uint32_t o = va_arg(args, uint32_t);
					puts(str_itoa_oct(o));
					break;
				case 'c':
					char c = va_arg(args, int);
					putc(c);
					break;
				case 's':
					const char *str = va_arg(args, const char*);
					puts(str);
					break;
			}
			++i;
		}else{
			//putc('!');
			putc(fmt[i]);
		}
	}

	va_end(args);
	return i;
}
