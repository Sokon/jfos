CC=i686-elf-gcc
AS=nasm
CFLAGS=-std=gnu99 -ffreestanding -O2 -Wall -Wextra
LDFLAGS=-T linker.ld -ffreestanding -O2 -nostdlib -lgcc
ASFLAGS=-f elf32

.PHONY: all
all: kern.bin
	mkdir -p build/boot/grub
	cp kern.bin build/boot/kern.bin
	cp grub.cfg build/boot/grub/grub.cfg
	grub-mkrescue -o build.iso build

kern.bin: boot.o kernel.o iolib.o stringlib.o infolib.o aux.o
	${CC} ${LDFLAGS} -o kern.bin boot.o kernel.o iolib.o stringlib.o infolib.o aux.o

kernel.o: kernel.c revision.h

revision.h: revision revision_template.h
	sed -e"s/NUMBER/`cat revision`/" revision_template.h > revision.h
	echo $$((`cat revision` + 1)) > revision

boot.o: boot.asm
	${AS} ${ASFLAGS} boot.asm

iolib.o: iolib.c

stringlib.o: stringlib.c

infolib.o: infolib.c

aux.o: aux.asm 
	${AS} ${ASFLAGS} aux.asm

.PHONY: clean
clean:
	rm -rf kern.bin *.o build.iso build/ revision.h

.PHONY: run
run: 
	vncviewer [::1]:5900 &
	qemu-system-i386 -cdrom build.iso -monitor stdio
.PHONY: re
re: clean all run
