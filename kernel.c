#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <cpuid.h>

#include "iolib.h"
#include "stringlib.h"
#include "revision.h"
#include "kernel.h"
#include "aux.h"
#include "infolib.h"
#include "multiboot.h"

#if defined(__linux__)
#error "Use a x-compiler fag"
#endif

#if !defined(__i386__)
#error "Use a _proper_ i386 x-compiler ya daft cunt"
#endif 

uint32_t cpu_features = 0;

extern uint16_t io_vga_buffer_width;
extern uint16_t io_vga_buffer_height;

void bbe(char *errormsg); 
void print_regs(void);

void kernel(multiboot_info_t *mbd, uint32_t magic) {
	cls();
	print("       _ ____\n      (_) __/\n     / / /_    jumpForth Operating \
System\n    / / __/    v~d.~d rev. ~d\n __/ /_/     \n/___/        \n\n",
		OSMAJOR, OSMINOR, REVISION);
	//puts(OSVERSION);
	//puts(" rev. ");
	//puts(REVISION);
	//puts("\n __/ /_/     \n/___/        \n\n");
	puts("CPU: ");
	puts(info_get_cpu_model());
	putc('\n');

	cpu_features = info_get_cpu_features();
	uint32_t prev = cpu_features;
	for(int i=0; i<=31; i++){
		if((prev & 0x01) == 1){
			puts(info_cpu_features[i]);
			putc(' ');
		}
		prev >>= 1;
	}
	putc('\n');
	print("~xloremipsum~d~s\n", 0x69, 2137, "chujkurwa");

	if(magic != MULTIBOOT_BOOTLOADER_MAGIC) {
		bbe("MULTIBOOT MAGIC VALUE INCORRECT, CANNOT DETECT RAM\n");
		return;
	}

	print_regs();
}

void bbe(char *errormsg) { /* Big Bad Error */
	io_set_buffer_colors(WHITE, RED);
	cls();
	uint32_t len = str_get_strlen(errormsg);
	io_set_buffer_colors(RED, WHITE);
	io_mov_cursor((io_vga_buffer_width>>1)-(7>>1), (io_vga_buffer_height>>1)-1);
	puts(" PANIC ");
	io_set_buffer_colors(WHITE, RED);
	io_mov_cursor((io_vga_buffer_width>>1)-(len>>1), 
			(io_vga_buffer_height>>1)+1);
	puts(errormsg);
	io_dis_cursor();
}

void print_regs(void){
	uint32_t r[10];
	asm volatile (
			"movl %%eax, %0\n\
			 movl %%ebx, %1\n\
			 movl %%ecx, %2\n\
			 movl %%edx, %3\n\
			 movl %%esi, %4\n\
			 movl %%edi, %5\n\
			 "
			: "=r" (r[0]), "=r" (r[1]), "=r" (r[2]), "=r" (r[3]), "=r" (r[4]), "=r" (r[5])
			:
			: 
		     );
	asm volatile (
			"movl %%esp, %0\n\
			 movl %%ebp, %1\n\
			"
			: "=r" (r[6]), "=r" (r[7])
			:
			:
		     );
}
