BITS 32
GLOBAL _start
EXTERN kernel

ALIGN_FLAG	equ 1<<0
MEMINFO_FLAG	equ 1<<1
FLAGS		equ ALIGN_FLAG | MEMINFO_FLAG
MAGIC		equ 0x1badb002
CHECKSUM	equ -(MAGIC+FLAGS)

SECTION .multiboot
	ALIGN 4
	dd MAGIC 
	dd FLAGS 
	dd CHECKSUM

SECTION .bss
	ALIGN 16
stack_bot:	resb 16384
stack_top:

SECTION .text
_start:
	mov 	esp, stack_top
	push	eax
	push	ebx
	call	kernel

	cli
halting_loop:
	hlt
	jmp halting_loop
