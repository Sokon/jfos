#include "stringlib.h"
#include <stdint.h>
#include <stddef.h>

/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 */
char* str_itoa(int value, char* result, int base) {
	// check that the base if valid
	if (base < 2 || base > 36) { *result = '\0'; return result; }

	char* ptr = result, *ptr1 = result, tmp_char;
	int tmp_value;

	do {
		tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	} while ( value );

	// Apply negative sign
	if (tmp_value < 0) *ptr++ = '-';
	*ptr-- = '\0';
	while(ptr1 < ptr) {
		tmp_char = *ptr;
		*ptr--= *ptr1;
		*ptr1++ = tmp_char;
	}
	return result;
}

char *str_itoa_dec(uint32_t value){
	return str_itoa(value, NULL, 10);
}

char *str_itoa_hex(uint32_t value){
	return str_itoa(value, NULL, 16);
}

char *str_itoa_oct(uint32_t value){
	return str_itoa(value, NULL, 8);
}
char *str_itoa_bin(uint32_t value){
	return str_itoa(value, NULL, 2);
}

uint32_t str_get_strlen(char *string){
	int i = 0;
	for(i; string[i] != '\0'; i++){}
	return i;
}
