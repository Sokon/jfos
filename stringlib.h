#ifndef __STRINGLIB_H__
#define __STRINGLIB_H__

#include <stdint.h>

char* str_itoa(int value, char* result, int base);
char *str_itoa_dec(uint32_t value);
char *str_itoa_hex(uint32_t value);
char *str_itoa_oct(uint32_t value);
char *str_itoa_bin(uint32_t value);
uint32_t str_get_strlen(char *string);

/* convenience macros */
#define dec(x) str_itoa_dec(x)
#define hex(x) str_itoa_hex(x)
#define oct(x) str_itoa_oct(x)
#define bin(x) str_itoa_bin(x)

#endif
